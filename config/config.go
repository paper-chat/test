package config

import "go.uber.org/fx"

type Config struct {
	ServerID  string
	MaxGuilds uint16
	DBType    string // Need to be rewrited to enums
}

var Module = fx.Options(
	fx.Provide(newConfig),
)

func newConfig() *Config {
	return &Config{ServerID: "http/localhost!80", MaxGuilds: 150, DBType: "gorm"}
}
