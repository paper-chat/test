package utils

func (u *Utils) GenerateID() uint64 {
	return uint64(u.sfnode.Generate().Int64())
}
