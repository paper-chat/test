package utils

import (
	"github.com/bwmarrin/snowflake"
	"gitlab.com/paper-chat/test/config"
	"go.uber.org/fx"
)

type Utils struct {
	sfnode *snowflake.Node
	config *config.Config
}

var Module = fx.Options(
	fx.Provide(newUtils),
)

func newUtils(sfnode *snowflake.Node, config *config.Config) *Utils {
	return &Utils{sfnode: sfnode, config: config}
}
