Messages
========

GET /guild/{guild_id}/channel/{channel_id}/msg?start={last_message_id}&count={message_count}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Return messages. If start={last_message_id} not specified, starts with last messages.
First message in list - last message in chhannel.

Reponse:

.. code-block:: JSON

  [
    {
      "id": "9284982398198",
      "author": "96248399231@paper.org",
      "content": "Hello!"
    },
    {
      "id": "9284982398204",
      "author": "96248398728@paper.org",
      "content": "Hello 2!"
    }
  ]

POST /guild/{guild_id}/channel/{channel_id}/msg
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Send message to specified channel. If "reply" not specified, message will
not be replied.

Request:

.. code-block:: JSON

  {
    "reply": "9819481948148",
    "content": "Hello!!!"
  }

Response: result status code

DELETE /guild/{guild_id}/channel/{channel_id}/msg/{message_id}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Deletes specified message.

Reponse: only status code. 200: deleted, 404: not found/you don't have access to guild or channel, 403: no permissions.

PUT /guild/{guild_id}/channel/{channel_id}/msg/{message_id}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Edit message. Only authors can edit their message. Not specified fields will not be edited.

Request:

.. code-block:: JSON

  {
    "reply": "937239852959",
    "content": "Hello 3"
  }

Response: 200: updated, 404: not found/you don't have access to guild, 403: you are
not message author.

