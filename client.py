from hashlib import sha256
import requests
import random, string
import base64
import hashlib

def get_server(id: string):
#    protoindex = id.find("/")
#    if id[:protoindex] == "http":
#       proto = "http"
#    elif id[:protoindex] == "https":
#       proto = "https"
   
#    portindex = id.find("!")
#    port = str(int(id[portindex + 1:]))
#    address = id[protoindex + 1:portindex]
#    r = requests.get(f"{proto}://{address}:{port}/.well-known/paper-chat/server.json")
#    return random.choice(r.json()['servers'])
    return "http://localhost:8080"

def randomword():
   letters = string.ascii_letters
   return ''.join(random.choice(letters) for i in range(8))

while True:
    email = randomword()
    original_password = randomword()
    username = randomword()
    target_server_id = "http/localhost!80"

    server_address = get_server(target_server_id)
    print(f"Got server address: {server_address}")

    password_hash = sha256(original_password.encode('utf-8')).hexdigest()
    password_for_server = sha256(f"{password_hash}salt{target_server_id}".encode('utf-8')).hexdigest()

    # Создание аккаунта
    r = requests.post(f'{server_address}/register', json={"email": email, "password": password_hash, "username": username})
    account_id = r.json()['id']
    print(f"Created account id: {account_id}, email: {email}, password: {password_hash}, username: {username}")

    gname = randomword()
    gdesc = randomword()

    # Создание гильдии
    r = requests.post(f'{server_address}/guilds', json={"name": gname, "description": gdesc}, auth=(f"{account_id}@{target_server_id}", password_for_server))
    guild_id = r.json()['id']
    print(f"Created guild with id: {guild_id}, name: {gname}, description: {gdesc}")
    r = requests.post(f'{server_address}/me/guilds', json={"id": guild_id, "server": target_server_id}, auth=(f"{account_id}@{target_server_id}", password_for_server))

    # Создание приглашения
    r = requests.post(f'{server_address}/guilds/{guild_id}/invites', json={"maxuses": 1024}, auth=(f"{account_id}@{target_server_id}", password_for_server))
    invite_id = r.json()['id']
    invite_code = r.json()['code']
    print(f"Created invite with id: {invite_id}, code: {invite_code}")

    # Получение информации о приглашении
    r = requests.get(f'{server_address}/invite/{invite_code}', auth=(f"{account_id}@{target_server_id}", password_for_server))
    print(f"Invite info for invite {invite_code}:")
    print(r.json())

    catname = randomword()
    catdesk = randomword()

    # Создание категории
    r = requests.post(f'{server_address}/guilds/{guild_id}/categories', json={"name": catname, "description": catdesk}, auth=(f"{account_id}@{target_server_id}", password_for_server))
    category_id = r.json()['id']
    print(f"Created category with id: {category_id}, name: {catname}, description: {catdesk}")

    # Создание канала
    cname = randomword()
    cdesc = randomword()
    r = requests.post(f'{server_address}/guilds/{guild_id}/channels', json={"name": cname, "description": cdesc, "category": category_id}, auth=(f"{account_id}@{target_server_id}", password_for_server))
    channel_id = r.json()['id']
    print(f"Created channel with id: {channel_id}, name: {cname}, description: {cdesc}, category: {category_id}")

    # Отправка сообщения
    content = randomword()
    r = requests.post(f'{server_address}/guilds/{guild_id}/channels/{channel_id}/messages', json={"content": content}, auth=(f"{account_id}@{target_server_id}", password_for_server))
    message_id = r.json()['id']
    print(f"Created message with id: {message_id}, content: {content}")

    # Получение сообщений
    r = requests.get(f'{server_address}/guilds/{guild_id}/channels/{channel_id}/messages', auth=(f"{account_id}@{target_server_id}", password_for_server))
    print(f"Messages for channel {channel_id}:")
    print(r.json())

    # Получение каналов
    r = requests.get(f'{server_address}/guilds/{guild_id}/channels', auth=(f"{account_id}@{target_server_id}", password_for_server))
    print(f"Channels for guild {guild_id}:")
    print(r.json())

    # Получение всех гильдий
    r = requests.get(f'{server_address}/guilds', auth=(f"{account_id}@{target_server_id}", password_for_server))
    print("My guilds:")
    print(r.json())
    break
