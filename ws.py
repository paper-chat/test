import asyncio
from websockets import connect

from hashlib import sha256
import requests
import string
from base64 import b64encode
import random

def get_server(id):
    return "http://localhost:8080"

def randomword():
   letters = string.ascii_letters
   return ''.join(random.choice(letters) for i in range(8))

email = randomword()
original_password = randomword()
username = randomword()
target_server_id = "http/localhost!80"
server_address = get_server(target_server_id)
print(f"Got server address: {server_address}")

password_hash = sha256(original_password.encode('utf-8')).hexdigest()
master_key = sha256(original_password.encode('utf-8')).hexdigest()
password_for_server = sha256(f"{password_hash}salt{target_server_id}".encode('utf-8')).hexdigest()

r = requests.post(f'{server_address}/register', json={"email": email, "password": password_hash, "username": username})
account_id = r.json()['id']
print(f"Created account id: {account_id}, email: {email}, password: {password_hash}, username: {username}")


def basic_auth_header(username, password):
    assert ':' not in username
    user_pass = f'{username}:{password}'
    basic_credentials = b64encode(user_pass.encode()).decode()
    return ('Authorization', f'Basic {basic_credentials}')

async def main(uri):
    async with connect(uri, extra_headers=[basic_auth_header(f"{account_id}@{target_server_id}", password_for_server)]) as websocket:
        print("Connected!")
        await websocket.recv()
        while True:
            result = await ws.recv()
            print(result)

asyncio.run(main(f"ws://127.0.0.1:8080/ws"))
