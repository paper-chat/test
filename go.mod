module gitlab.com/paper-chat/test

go 1.16

require (
	github.com/Depado/ginprom v1.7.1
	github.com/bwmarrin/snowflake v0.3.0
	github.com/gin-gonic/gin v1.7.2
	github.com/gorilla/websocket v1.4.2
	github.com/prometheus/client_golang v1.11.0
	github.com/sirupsen/logrus v1.8.1
	go.uber.org/fx v1.13.1
	gorm.io/driver/sqlite v1.1.4
	gorm.io/gorm v1.21.11
)
