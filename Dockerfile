FROM golang:1.16 AS builder

WORKDIR /usr/src/app

COPY . .
RUN CGO_ENABLED=1 go build -v -o paper-server

FROM debian:10-slim

WORKDIR /usr/local/bin

COPY --from=builder /usr/src/app/paper-server .
ENTRYPOINT ["/usr/local/bin/paper-server"]
