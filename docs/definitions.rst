Server ID - protocol + servers's defenition address, for example "https:paper-server.org" or "paper-server.org".
Also, you can use IPv6: "http:[200:b2d8:44c0:1a7f:bb72:65ce:9dbf:2902]" or "[200:b2d8:44c0:1a7f:bb72:65ce:9dbf:2902]".
When protocol not specified, it defaultsto https for domain names and http for ip addresses.

.. warning::
    "https:paper-server.org" and "paper-server.org" will stored as completely
    different servers in databases, so, if user from "paper-server.org" exist in guild, the same user from
    "https:paper-server.org" will not have access to guild. If you use default protocol (https for domains
    and http for ip addresses, don't specify protocol!

UID (Unical ID) - we use snowflake-like numeric uint64 increasing over time ids. Users, guilds, channels, etc have
it's own unical id.
