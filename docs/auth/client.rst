Client-server authorization
===========================

Client-server authentication is simple: first, client provide login credentials to homeserver and
get access token from it. Then we use basic auth: login is client's id, and password is SHA256 hash of
token + server id, server send request to homeserver and authorize user. Now let's look at how this
works in more detail.

Use your ID as username and SHA256(token + server id) as password for basic authentication.
Server will response 401 if it can't verify your auth info or normally process your request.

POST /login
~~~~~~~~~~~

Generates client access token, save token and id somewhere.

Request:

.. code-block:: JSON

  {
    "email": "example@example.org",
    "password": "VeryStrongPassword",
    "type": "PyPaper Client 0.0.1"
  }

Response:

.. code-block:: JSON

  {
    "id": "717093966922907709",
    "token": "eCxkCJ9mrE3bFdLAeVHjZLZXnhKtBbiO"
  }

GET /tokens
~~~~~~~~~~~

Responses with list of all your tokens and info about them. Use to check authorizations and
then revoke any of them.

Request: no query params needed.

Reponse:

.. code-block:: JSON

  [
    {
      "token": "zqdAlbnaXvhoxIYwjF1LhpUwNoz5EMh4",
      "client": "Pyton Paper library 0.0.1",
      "type": "PyPaper Desktop Client 0.0.1",
      "origin": null
    },
    {
      "token": "RuGqUxn47tQxllHrgGakim7WBN6TUh6d",
      "client": "Mozilla/5.0 (X11; Linux x86_64; rv:88.0) Gecko/20100101 Firefox/88.0",
      "type": "PyPaper Client 0.0.1",
      "origin": "https://online.paper-client.org"
    }
  ]

DELETE /tokens/{token}
~~~~~~~~~~~~~~~~~~~~~~

Revokes {token} token. Use "current" as {token} value to revoke current session token.

Request: no request body needed, only authorization info.

Reponse: only status code.
