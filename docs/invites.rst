Invites
=======

POST /guild/{guild_id}/invite
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Creates new invite to guild to specified channel and with specified expiration time in unix-time format.

Request:

.. code-block:: JSON

  {
    "channel": "14640619082",
    "expires": "1619789233"
  }

Response: status code.

GET /guild/{guild_id}/invite
~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Responses with invites list and "type": "all" if you have permission "VIEW_INVITES", and only your with "type": "your" otherwise

Reponse:

.. code-block:: JSON

  {
    "type": "all",
    "invites": [
        {
          "id": "798147212717",
          "channel": "14640619082",
          "expires": "1619789233",
          "user": "9818461928@example.org"
        },
        {
          "id": "618467124120",
          "channel": "16984934614",
          "expires": "1625059633",
          "user": "1741284981@paper.org"
        }
    ]
  }

DELETE /guild/{guild_id}/invite/{invite_id}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Revokes invite.

Reponse: only status code. 200: deleted, 404: not found/you don't have access to guild, 403: you can't revoke
someone else's invite.
