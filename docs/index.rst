Welcome to Paper Chat wiki!
===========================

What is it?
-----------
It is a free, open source, decentralized inernet messaging protocol and it's realization.
It is like matrix, but developed specially for big public (and private of course) guilds.
Similar to discord, each guild has as many as you want channels. Each user in guild can
have multiple roles with permissions. Role's permissions sets globally, but can be overriden
per channel.

Project status
--------------
Currentry we developing protocol.

.. toctree::
    :maxdepth: 2
    :hidden:
    :caption: Overview
 
    /defenitions

.. toctree::
    :maxdepth: 2
    :hidden:
    :caption: Authentication
 
    /auth/client
    /auth/server

    .. toctree::
    :maxdepth: 2
    :hidden:
    :caption: API
 
    /guilds
    /channels
    /invites
    /channels
    /categories
