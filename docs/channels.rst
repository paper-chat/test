Channels
========

GET /guild/{guild_id}/channel
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Return list of all your channels on target guild.

Reponse:

.. code-block:: JSON

  [
    {
      "name": "coding",
      "category": "23783322313",
      "id": "372983673328342349"
    },
    {
      "name": "off-topic",
      "category": "29382923843",
      "id": "138924892891038443"
    }
  ]

POST /guild/{guild_id}/channel
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Creates new channel on target guild. Category is optional.

Request:

.. code-block:: JSON

  {
    "name": "rules",
    "category": "1232115321"
  }

.. tip::
  If you don't want to assign any category to new channel, you can either left "category" option out or implicitly set it's value to null:

  .. code-block:: JSON

    {
      "name": "rules"
    }

  Or:

  .. code-block:: JSON

    {
      "name": "rules",
      "category": null
    }

Response:

.. code-block:: JSON

  {
    "id": "324932490298342394"
  }

GET /guild/{guild_id}/channel/{channel_id}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Responses with requested channel info. If you don't have access to channel or there is no
such channel on target guild, you will get 404.

Reponse:

.. code-block:: JSON

  {
    "name": "coding",
    "category": "23783322313"
  }

DELETE /guild/{guild_id}/channel/{channel_id}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Deletes specified channel. This operation can perform only users with "MANAGE_CHANNELS" permission.

Reponse: only status code. 200: deleted, 404: not found/you don't have access to channel, 403: you don't have "MANAGE_CHANNELS" permission.

PUT /guild/{guild_id}/channel/{channel_id}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Updates channel's info. This operation can perform only owner and users with "MANAGE_CHANNELS" permission.

Request:

.. code-block:: JSON

  {
    "name": "golang-coding",
    "category": "43534442435"
  }

.. tip::
  You don't have to specify all options:
  
  .. code-block:: JSON

    {
      "category": "43534442435"
    }

Response: 200: updated, 404: not found/you don't have access to channel, 403: you don't have "MANAGE_CHANNELS" permission.

