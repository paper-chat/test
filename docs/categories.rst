Categories
==========

GET /guild/{guild_id}/category
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Return list of all categories in a guild.

Reponse:

.. code-block:: JSON

  [
    {
      "name": "Development",
      "id": "6823601608012"
    },
     {
      "name": "Testing",
      "id": "36498649820235"
    }
  ]

POST /guild/{guild_id}/category
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Creates new channel category in a guild.

Request:

.. code-block:: JSON

  {
    "name": "Development"
  }

Response:

.. code-block:: JSON

  {
    "id": "98164681601386"
  }

GET /guild/{guild_id}/category/{category_id}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Responses with requested category info. If you don't have access to guild or there is no
such category on target server, you will get 404.

Reponse:

.. code-block:: JSON

  {
    "name": "Development"
  }

DELETE /guild/{guild_id}/category/{category_id}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Deletes specified category. You must have "MANAGE_CATEGORIES" permission.

Reponse: only status code. 200: deleted, 404: not found/you don't have access to guild, 403: no permission.

PUT /guild/{guild_id}/category/{category_id}
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

Updates category's info. You must have "MANAGE_CATEGORIES" permission.

Request:

.. code-block:: JSON

  {
    "name": "Backend"
  }

Response: 200: updated, 404: not found/you don't have access to guild, 403: you don't have "MANAGE_CATEGORIES" permission.
