Guilds
======

GET /guild
~~~~~~~~~~

Return list of all your guilds on target server.

Reponse:

.. code-block:: JSON

  [
    {
      "name": "Paper Development",
      "owner": "61698146981@paper.org",
      "members": "1500"
    },
    {
      "name": "Paper Development 2",
      "owner": "164639481151@paper2.org",
      "members": "7"
    }
  ]

POST /guild
~~~~~~~~~~~

Creates new guild on target server, your account is guild's owner. You can create new guild on any server,
not only on homeserver!

Request:

.. code-block:: JSON

  {
    "name": "Paper Development"
  }

Response:

.. code-block:: JSON

  {
    "id": "717093966922907709"
  }

GET /guild/{id}
~~~~~~~~~~~~~~~

Responses with requested guild info. If you don't have access to guild or there is no
such guild on target server, you will get 404.

Reponse:

.. code-block:: JSON

  {
    "name": "Paper Development",
    "owner": "61698146981@paper.org",
    "members": "1500"
  }

DELETE /guild/{id}
~~~~~~~~~~~~~~~~~~~~~~

Deletes specified guild. This operation can perform only guild's owner.

Reponse: only status code. 200: deleted, 404: not found/you don't have access to guild, 403: you are not owner.

PUT /guild/{id}
~~~~~~~~~~~~~~~

Updates guild's info. This operation can perform only owner and users with "CHANGE_GUILD_INFO" permission.

Request:

.. code-block:: JSON

  {
    "name": "Paper Development 2"
  }

Response: 200: updated, 404: not found/you don't have access to guild, 403: you don't have "CHANGE_GUILD_INFO" permission.
