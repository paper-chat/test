package types

type Message struct {
	ID           uint64 `json:"id"`
	AuthorID     uint64 `json:"authorid"`
	AuthorServer string `json:"authorserver"`
	ChannelID    uint64 `json:"channel"`
	Content      string `json:"content"`
	ReplyToID    uint64 `json:"replyto"`
}
