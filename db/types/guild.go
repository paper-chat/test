package types

type GuildInfo struct {
	ID          uint64
	Name        string
	Description string
}

type ChannelInfo struct {
	ID          uint64
	Name        string
	Description string
	Category    uint64
}
