package gormdb

type Category struct {
	ID          uint64 `gorm:"primaryKey;autoIncrement:false"`
	GuildID     uint64 `gorm:"index"`
	Name        string
	Description string
}

func (g *GormDB) DeleteChannel(id uint64) error {
	g.db.Delete(&Channel{}, id)
	return nil
}

func (g *GormDB) CreateCategory(guild uint64, name string, description string) uint64 {
	id := g.utils.GenerateID()
	g.db.Create(&Category{ID: id, GuildID: guild, Name: name, Description: description})
	return id
}

func (g *GormDB) DeleteCategory(id uint64) error {
	g.db.Delete(&Category{}, id)
	return nil
}
