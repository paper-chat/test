package gormdb

import (
	"gitlab.com/paper-chat/test/db/types"
	"gorm.io/gorm"
)

type Message struct {
	ID           uint64 `gorm:"primaryKey;autoIncrement:false;index:idx_messages,priority:2,sort:desc"`
	AuthorID     uint64
	AuthorServer string
	ChannelID    uint64 `gorm:"index:idx_messages,priority:1"`
	Content      string
	ReplyToID    uint64
}

func (message *Message) BeforeDelete(tx *gorm.DB) (err error) {
	tx.Where(&Reaction{MessageID: message.ID}).Delete(&Reaction{})
	return
}

func (g *GormDB) GetMessages(channel uint64, count int, from uint64) *[]types.Message {
	var messages []types.Message
	if from != 0 {
		g.db.Where(&Message{ChannelID: channel}).Where("id > ?", from).Limit(count).Find(&messages)
	} else {
		g.db.Where(&Message{ChannelID: channel}).Limit(count).Find(&messages)
	}
	var retmsgs []types.Message
	for _, msg := range messages {
		retmsgs = append(retmsgs, types.Message{ID: msg.ID, AuthorID: msg.AuthorID, AuthorServer: msg.AuthorServer, ChannelID: msg.ChannelID, Content: msg.Content, ReplyToID: msg.ReplyToID})
	}
	return &retmsgs
}

func (g *GormDB) CreateMessage(authorid uint64, authorserver string, channel uint64, content string) uint64 {
	id := g.utils.GenerateID()
	go g.db.Create(&Message{ID: id, AuthorID: authorid, AuthorServer: authorserver, ChannelID: channel, Content: content})
	return id
}

func (g *GormDB) DeleteMessage(id uint64) error {
	g.db.Delete(&Message{}, id)
	return nil
}

func (g *GormDB) UpdateMessage(id uint64, content string) error {
	g.db.Model(&Message{}).Where(&Message{ID: id}).Updates(&Message{Content: content})
	return nil
}
