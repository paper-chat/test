package gormdb

import "time"

type UserGuild struct {
	UserID     uint64 `gorm:"index:idx_user_guilds_users"`
	UserServer string `gorm:"index:idx_user_guilds_users"`
	GuildID    uint64 `gorm:"index:idx_user_guilds_guilds"`
	Joined     time.Time
}

type MyUserGuild struct {
	UserID      uint64 `gorm:"index:idx_my_user_guilds"`
	GuildServer string `gorm:"index:idx_my_user_guilds"`
	GuildID     uint64 `gorm:"index:idx_my_user_guilds"`
	Key         string
}

func (g *GormDB) SaveUsersGuild(user uint64, guild uint64, server string, key string) error {
	g.db.Create(&MyUserGuild{UserID: user, GuildID: guild, GuildServer: server, Key: key})
	return nil
}

func (g *GormDB) GetGuildsForUser(userid uint64, userserver string) []uint64 {
	var guilds []UserGuild
	g.db.Where(&UserGuild{UserID: userid, UserServer: userserver}).Find(&guilds)
	var ids []uint64
	for _, guild := range guilds {
		ids = append(ids, guild.GuildID)
	}
	return ids
}

func (g *GormDB) IsUserInGuild(userid uint64, userserver string, guild uint64) bool {
	var guilds []UserGuild
	g.db.Where(&UserGuild{UserID: userid, UserServer: userserver, GuildID: guild}).Limit(1).Find(&guilds)
	if len(guilds) == 1 && guild != 0 {
		return true
	} else {
		return false
	}
}

func (g *GormDB) AddUserToGuild(userid uint64, userserver string, guild uint64) error {
	g.db.Create(&UserGuild{UserID: userid, UserServer: userserver, GuildID: guild, Joined: time.Now()})
	return nil
}
