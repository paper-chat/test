package gormdb

type Reaction struct {
	ReactorID     uint64
	ReactorServer string
	EmojiID       uint64
	MessageID     uint64 `gorm:"index"`
}
