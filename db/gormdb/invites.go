package gormdb

type Invite struct {
	ID            uint64 `gorm:"primaryKey;autoIncrement:false"`
	Code          string `gorm:"index:idx_invites_code;type:char(8)"`
	GuildID       uint64 `gorm:"index:idx_invites_guild"`
	InviterID     uint64
	InviterServer string
	Uses          uint32
	MaxUses       uint32
	// ValidUntil    time.Time
}

func (g *GormDB) CreateInvite(guild uint64, inviterid uint64, inviterserver string, maxuses uint32) (uint64, string) { // ID, Code
	id := g.utils.GenerateID()
	code := g.utils.RandStringRunes(8)
	g.db.Create(&Invite{ID: id, Code: code, GuildID: guild, InviterID: inviterid, InviterServer: inviterserver, Uses: 0, MaxUses: maxuses})
	return id, code
}

func (g *GormDB) DeleteInvite(id uint64) error {
	g.db.Delete(&Invite{}, id)
	return nil
}

func (g *GormDB) GetInviteByCode(code string) (uint64, uint64, uint64, string, uint32, uint32) { // ID, Guild, InviterID, InviterServer, Uses, MaxUses
	var invites []Invite
	g.db.Where(&Invite{Code: code}).Limit(1).Find(&invites)
	if len(invites) == 1 {
		return invites[0].ID, invites[0].GuildID, invites[0].InviterID, invites[0].InviterServer, invites[0].Uses, invites[0].MaxUses
	} else {
		return 0, 0, 0, "", 0, 0
	}
}
