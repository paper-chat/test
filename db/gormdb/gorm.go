package gormdb

import (
	"gitlab.com/paper-chat/test/config"
	"gitlab.com/paper-chat/test/utils"
	"gorm.io/driver/sqlite"
	"gorm.io/gorm"
)

type GormDB struct {
	db     *gorm.DB
	utils  *utils.Utils
	config *config.Config
}

func NewGormDB(utils *utils.Utils, config *config.Config) *GormDB {
	db, err := gorm.Open(sqlite.Open("./test.db"), &gorm.Config{})
	if err != nil {
		panic(err)
	}
	db.AutoMigrate(&User{}, &Guild{}, &Channel{}, &Message{}, &Emoji{}, &Reaction{}, &UserGuild{}, &MyUserGuild{}, &Invite{}, &Category{})
	return &GormDB{db: db, utils: utils, config: config}
}
