package gormdb

import (
	"gitlab.com/paper-chat/test/db/types"
	"gorm.io/gorm"
)

type Channel struct {
	ID          uint64 `gorm:"primaryKey;autoIncrement:false"`
	GuildID     uint64 `gorm:"index"`
	Name        string
	Description string
	Category    uint64
}

func (channel *Channel) BeforeDelete(tx *gorm.DB) (err error) {
	tx.Where(&Message{ChannelID: channel.ID}).Delete(&Message{})
	return
}

func (g *GormDB) CreateChannel(guild uint64, name string, description string, category uint64) uint64 {
	id := g.utils.GenerateID()
	g.db.Create(&Channel{ID: id, GuildID: guild, Name: name, Description: description, Category: category})
	return id
}

func (g *GormDB) GetChannelsForGuild(guildid uint64) []uint64 {
	var channels []Channel
	g.db.Where(&Channel{GuildID: guildid}).Find(&channels)
	var ids []uint64
	for _, channel := range channels {
		ids = append(ids, channel.ID)
	}
	return ids
}

func (g *GormDB) GetChannelInfo(id uint64) *types.ChannelInfo {
	var channel Channel
	g.db.Where(&Channel{ID: id}).Find(&channel)
	if channel.ID == 0 {
		return nil
	} else {
		return &types.ChannelInfo{ID: id, Name: channel.Name, Description: channel.Description, Category: channel.Category}
	}
}
