package gormdb

import (
	"crypto/sha256"
	"encoding/hex"

	"gitlab.com/paper-chat/test/db/types"
)

type User struct {
	ID       uint64 `gorm:"primaryKey;autoIncrement:false"`
	Email    string `gorm:"uniqueIndex"`
	Username string
	Password string `gorm:"type:char(64)"`
}

func (g *GormDB) CreateUser(username string, password string, email string) uint64 {
	id := g.utils.GenerateID()
	g.db.Create(&User{ID: id, Email: email, Password: password, Username: username})
	return id
}

func (g *GormDB) GetUserByCredentials(email string, password string) *types.User {
	var users []User
	hash := sha256.Sum256([]byte(password))
	g.db.Where(&User{Email: email, Password: hex.EncodeToString(hash[:])}).Limit(1).Find(&users)
	if len(users) == 1 {
		return &types.User{ID: users[0].ID}
	} else {
		return &types.User{ID: 0}
	}
}

func (g *GormDB) IsPasswordValid(user uint64, password string) bool {
	var users []User
	g.db.Where(&User{ID: user}).Limit(1).Find(&users)
	if len(users) == 1 {
		hash := sha256.Sum256([]byte(users[0].Password + "salt" + g.config.ServerID))
		encoded := hex.EncodeToString(hash[:])
		return encoded == password
	} else {
		return false
	}
}
