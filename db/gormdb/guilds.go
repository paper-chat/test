package gormdb

import (
	"time"

	"gitlab.com/paper-chat/test/db/types"
	"gorm.io/gorm"
)

type Guild struct {
	ID          uint64 `gorm:"primaryKey;autoIncrement:false"`
	Name        string
	Description string
	OwnerID     uint64
	OwnerServer string
}

func (guild *Guild) BeforeDelete(tx *gorm.DB) (err error) {
	tx.Where(&Channel{GuildID: guild.ID}).Delete(&Channel{})
	tx.Where(&Invite{GuildID: guild.ID}).Delete(&Invite{})
	tx.Where(&UserGuild{GuildID: guild.ID}).Delete(&UserGuild{})
	tx.Where(&Emoji{GuildID: guild.ID}).Delete(&Emoji{})
	return
}

func (g *GormDB) CreateGuild(name string, description string, ownerid uint64, ownerserver string) uint64 {
	id := g.utils.GenerateID()
	g.db.Create(&Guild{ID: id, Name: name, OwnerID: ownerid, OwnerServer: ownerserver, Description: description})
	g.db.Create(&UserGuild{UserID: ownerid, UserServer: ownerserver, GuildID: id, Joined: time.Now()})
	return id
}

func (g *GormDB) DeleteGuild(id uint64) error {
	g.db.Delete(&Guild{}, id)
	return nil
}

func (g *GormDB) UpdateGuild(id uint64, name string, description string) {
	g.db.Model(&Guild{}).Where(&Guild{ID: id}).Updates(&Guild{Name: name, Description: description})
}

func (g *GormDB) GetGuildInfo(id uint64) *types.GuildInfo {
	var guild Guild
	g.db.Where(&Guild{ID: id}).Find(&guild)
	return &types.GuildInfo{ID: id, Name: guild.Name, Description: guild.Description}
}
