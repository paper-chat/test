package gormdb

type Emoji struct {
	ID            uint64 `gorm:"primaryKey;autoIncrement:false"`
	CreatorID     uint64
	CreatorServer string
	GuildID       uint64
}
