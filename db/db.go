package db

import (
	"gitlab.com/paper-chat/test/config"
	"gitlab.com/paper-chat/test/db/gormdb"
	"gitlab.com/paper-chat/test/db/types"
	"gitlab.com/paper-chat/test/utils"
	"go.uber.org/fx"
)

type DB interface {
	CreateUser(username string, password string, email string) uint64
	GetUserByCredentials(email string, password string) *types.User
	CreateGuild(name string, description string, ownerid uint64, ownerserver string) uint64
	UpdateGuild(id uint64, name string, description string)
	AddUserToGuild(userid uint64, userserver string, guild uint64) error
	CreateChannel(guild uint64, name string, description string, category uint64) uint64
	CreateMessage(authorid uint64, authorserver string, channel uint64, content string) uint64
	DeleteMessage(id uint64) error
	GetMessages(channel uint64, count int, from uint64) *[]types.Message
	UpdateMessage(message uint64, content string) error
	DeleteChannel(id uint64) error
	DeleteGuild(id uint64) error
	GetGuildsForUser(userid uint64, userserver string) []uint64
	GetGuildInfo(id uint64) *types.GuildInfo
	IsUserInGuild(userid uint64, userserver string, guild uint64) bool
	IsPasswordValid(user uint64, password string) bool
	SaveUsersGuild(user uint64, guilds uint64, server string, key string) error
	CreateCategory(guild uint64, name string, description string) uint64
	DeleteCategory(id uint64) error
	GetChannelsForGuild(guildid uint64) []uint64
	GetChannelInfo(id uint64) *types.ChannelInfo
	CreateInvite(guild uint64, inviterid uint64, inviterserver string, maxuses uint32) (uint64, string)
	DeleteInvite(id uint64) error
	GetInviteByCode(code string) (uint64, uint64, uint64, string, uint32, uint32)
}

var Module = fx.Options(
	fx.Provide(newDB),
)

func newDB(utils *utils.Utils, config *config.Config) DB {
	if config.DBType == "gorm" {
		return gormdb.NewGormDB(utils, config)
	} else {
		panic("Unsupported db type: " + config.DBType)
	}
}
