package api

import (
	"context"
	"log"
	"net/http"
	"strconv"
	"strings"

	"github.com/Depado/ginprom"
	"github.com/bwmarrin/snowflake"
	"github.com/gin-gonic/gin"
	"gitlab.com/paper-chat/test/api/info"
	"gitlab.com/paper-chat/test/api/ws"
	"gitlab.com/paper-chat/test/config"
	"gitlab.com/paper-chat/test/db"
	"gitlab.com/paper-chat/test/utils"
	"go.uber.org/fx"
)

type Handler struct {
	db     db.DB
	hub    *ws.Hub
	sfnode *snowflake.Node
	utils  *utils.Utils
	config *config.Config
}

var Module = fx.Options(
	fx.Provide(newRouter),
	fx.Provide(newHandler),
)

func newRouter(lifecycle fx.Lifecycle) *gin.Engine {
	router := gin.Default()
	server := http.Server{
		Addr:    ":8080",
		Handler: router,
	}
	p := ginprom.New(
		ginprom.Engine(router),
		ginprom.Subsystem("gin"),
		ginprom.Path("/metrics"),
	)
	router.Use(p.Instrument())
	lifecycle.Append(
		fx.Hook{
			OnStart: func(context.Context) error {
				go server.ListenAndServe()
				return nil
			},
			OnStop: func(ctx context.Context) error {
				return server.Shutdown(ctx)
			},
		},
	)
	return router
}

func newHandler(logger *log.Logger, db db.DB, sfnode *snowflake.Node, utils *utils.Utils, config *config.Config) *Handler {
	hub := ws.NewHub(db)
	go hub.Run()
	h := &Handler{db: db, hub: hub, sfnode: sfnode, utils: utils, config: config}
	return h
}

func authMiddleware(db db.DB, config *config.Config) gin.HandlerFunc {
	return func(c *gin.Context) {
		user, password, ok := c.Request.BasicAuth()
		if ok {
			index := strings.Index(user, "@")
			if index == -1 {
				c.AbortWithStatus(401)
			}
			userid, err := strconv.ParseUint(user[0:index], 10, 64)
			userserver := user[index+1:]
			if userserver != config.ServerID {
				// Federation not implemented yet
				c.AbortWithStatus(401)
			}
			if err == nil {
				if db.IsPasswordValid(userid, password) {
					c.Set("user", &info.RequestInfo{ID: userid, Server: userserver})
					c.Next()
					return
				}
			}
			c.AbortWithStatus(401)
		}
		c.AbortWithStatus(401)
	}
}

func guildMiddleware(db db.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		guild := c.Param("guild")
		guildid, err := strconv.ParseUint(guild, 10, 64)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		user := c.MustGet("user").(*info.RequestInfo)
		if !db.IsUserInGuild(user.ID, user.Server, guildid) {
			c.AbortWithStatus(http.StatusNotFound)
			return
		}
		c.Set("guild", guildid)
		c.Next()
	}
}

func channelMiddleware(db db.DB) gin.HandlerFunc {
	return func(c *gin.Context) {
		channel := c.Param("channel")
		channelid, err := strconv.ParseUint(channel, 10, 64)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		if db.GetChannelInfo(channelid) == nil {
			c.AbortWithStatus(http.StatusNotFound)
			return
		}
		c.Set("channel", channelid)
		c.Next()
	}
}

func messageMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		message := c.Param("message")
		messageid, err := strconv.ParseUint(message, 10, 64)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		c.Set("message", messageid)
		c.Next()
	}
}

func categoryMiddleware() gin.HandlerFunc {
	return func(c *gin.Context) {
		category := c.Param("category")
		categoryid, err := strconv.ParseUint(category, 10, 64)
		if err != nil {
			c.AbortWithStatusJSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		c.Set("category", categoryid)
		c.Next()
	}
}

func RegisterHandlers(router *gin.Engine, h *Handler) {
	router.POST("/register", h.PerformRegister)

	authRequired := router.Group("/")
	authRequired.Use(authMiddleware(h.db, h.config))

	authRequired.POST("/guilds", h.PerformGuildCreation)        // Создание гильдии
	authRequired.GET("/guilds", h.PerformGetGuilds)             // Получение гильдий на этом сервере
	authRequired.GET("/invite/:invite", h.PerformGetInviteInfo) // Получение информации о приглашении

	guildRequired := authRequired.Group("/guilds/:guild")
	guildRequired.Use(guildMiddleware(h.db))
	guildRequired.DELETE("/", h.PerformDeleteGuild) // Удаление гильдии
	guildRequired.PUT("/", h.PerformUpdateGuild)    // Обновление гильдии

	guildRequired.POST("/channels", h.PerformCreateChannel) // Создание канала
	guildRequired.GET("/channels", h.PerformGetChannels)    // Получение каналов

	channelRequired := guildRequired.Group("/channels/:channel")
	channelRequired.Use(channelMiddleware(h.db))
	channelRequired.DELETE("/", h.PerformDeleteChannel)       // Удаление канала
	channelRequired.POST("/messages", h.PerformCreateMessage) // Отправка сообщения
	channelRequired.GET("/messages", h.PerformGetMessages)    // Получение сообщений

	messageRequired := channelRequired.Group("/messages/:message")
	messageRequired.Use(messageMiddleware())
	messageRequired.DELETE("/", h.PerformDeleteMessage) // Удаление сообщения
	messageRequired.PUT("/", h.PerformUpdateMessage)    // Изменение сообщений

	guildRequired.POST("/categories", h.PerformCreateCategory) // Создание категории
	guildRequired.POST("/invites", h.PerformInviteCreation)    // Создание приглашения

	categoryRequired := guildRequired.Group("/categories/:category")
	categoryRequired.Use(categoryMiddleware())
	categoryRequired.DELETE("/", h.PerformDeleteCategory) // Удаление категории

	authRequired.POST("/me/guilds", h.PerformSaveMyGuild) // Сохранение информации о гильдии у себя

	authRequired.GET("/ws", func(c *gin.Context) {
		user := c.MustGet("user").(*info.RequestInfo)
		ws.ServeWs(h.hub, c.Writer, c.Request, user)
	})
}
