package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/paper-chat/test/api/requests"
	res "gitlab.com/paper-chat/test/api/responses"
)

func (h *Handler) PerformCreateChannel(c *gin.Context) {
	var req requests.ChannelCreationRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	guild := c.MustGet("guild").(uint64)
	id := h.db.CreateChannel(guild, req.Name, req.Description, req.Category)
	c.JSON(200, &res.IDResponse{ID: id})
}

func (h *Handler) PerformDeleteChannel(c *gin.Context) {
	channel := c.MustGet("channel").(uint64)
	h.db.DeleteChannel(channel)
	c.Status(200)
}

func (h *Handler) PerformGetChannels(c *gin.Context) {
	guild := c.MustGet("guild").(uint64)
	ids := h.db.GetChannelsForGuild(guild)
	var channels []res.Channel
	for _, id := range ids {
		channelinfo := h.db.GetChannelInfo(id)
		channels = append(channels, res.Channel{ID: id, Name: channelinfo.Name, Description: channelinfo.Description, Category: channelinfo.Category})
	}
	c.JSON(200, channels)
}
