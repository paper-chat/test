package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	req "gitlab.com/paper-chat/test/api/requests"
	res "gitlab.com/paper-chat/test/api/responses"
)

func (h *Handler) PerformRegister(c *gin.Context) {
	var req req.AccountCreationRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	id := h.db.CreateUser(req.Username, req.Password, req.Email)
	c.JSON(200, &res.IDResponse{ID: id})
}
