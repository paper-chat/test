package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/paper-chat/test/api/info"
	req "gitlab.com/paper-chat/test/api/requests"
	res "gitlab.com/paper-chat/test/api/responses"
)

func (h *Handler) PerformGuildCreation(c *gin.Context) {
	var req req.GuildCreationRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	user := c.MustGet("user").(*info.RequestInfo)
	ids := h.db.GetGuildsForUser(user.ID, user.Server)
	if uint16(len(ids)) > h.config.MaxGuilds {
		c.JSON(200, &res.IDResponse{ID: 0})
	}
	id := h.db.CreateGuild(req.Name, req.Description, user.ID, user.Server)
	c.JSON(200, &res.IDResponse{ID: id})
}

func (h *Handler) PerformDeleteGuild(c *gin.Context) {
	user := c.MustGet("user").(*info.RequestInfo)
	guild := c.MustGet("guild").(uint64)
	if !h.db.IsUserInGuild(user.ID, user.Server, guild) {
		c.AbortWithStatus(404)
		return
	}
	h.db.DeleteGuild(guild)
	c.Status(200)

}

func (h *Handler) PerformUpdateGuild(c *gin.Context) {
	var req req.GuildUpdateRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	guild := c.MustGet("guild").(uint64)

	h.db.UpdateGuild(guild, req.Name, req.Description)
	c.Status(200)

}

func (h *Handler) PerformGetGuilds(c *gin.Context) {
	user := c.MustGet("user").(*info.RequestInfo)
	ids := h.db.GetGuildsForUser(user.ID, user.Server)
	var guilds []res.Guild
	for _, id := range ids {
		ginfo := h.db.GetGuildInfo(id)
		guilds = append(guilds, res.Guild{ID: id, Name: ginfo.Name, Description: ginfo.Description})
	}
	c.JSON(200, guilds)
}
