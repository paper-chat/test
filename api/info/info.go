package info

type RequestInfo struct {
	ID     uint64
	Server string
}
