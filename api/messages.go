package api

import (
	"net/http"
	"strconv"

	"github.com/gin-gonic/gin"
	"gitlab.com/paper-chat/test/api/info"
	req "gitlab.com/paper-chat/test/api/requests"
	res "gitlab.com/paper-chat/test/api/responses"
	"gitlab.com/paper-chat/test/api/ws"
)

func (h *Handler) PerformCreateMessage(c *gin.Context) {
	var req req.MessageCreationRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	user := c.MustGet("user").(*info.RequestInfo)
	guild := c.MustGet("guild").(uint64)
	channel := c.MustGet("channel").(uint64)
	id := h.db.CreateMessage(user.ID, user.Server, channel, req.Content)
	h.hub.NewMessage <- &ws.Message{ID: id, ChannelID: channel, GuildID: guild, AuthorID: user.ID, AuthorServer: user.Server, Content: req.Content}
	c.JSON(200, &res.IDResponse{ID: id})
}

func (h *Handler) PerformDeleteMessage(c *gin.Context) {
	message := c.MustGet("message").(uint64)
	h.db.DeleteMessage(message)
	c.Status(200)
}

func (h *Handler) PerformGetMessages(c *gin.Context) {
	from := c.DefaultQuery("from", "0")
	fromint, err := strconv.ParseUint(from, 10, 64)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	count := c.DefaultQuery("from", "0")
	countint, err := strconv.Atoi(count)
	if err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	channel := c.MustGet("channel").(uint64)
	msgs := h.db.GetMessages(channel, countint, fromint)
	c.JSON(200, msgs)
}

func (h *Handler) PerformUpdateMessage(c *gin.Context) {
	var req req.MessageUpdateRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	message := c.MustGet("message").(uint64)

	h.db.UpdateMessage(message, req.Content)
	c.Status(200)
}
