package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/paper-chat/test/api/info"
	req "gitlab.com/paper-chat/test/api/requests"
	res "gitlab.com/paper-chat/test/api/responses"
)

func (h *Handler) PerformInviteCreation(c *gin.Context) {
	var req req.InviteCreationRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	user := c.MustGet("user").(*info.RequestInfo)
	guild := c.MustGet("guild").(uint64)
	id, code := h.db.CreateInvite(guild, user.ID, user.Server, req.MaxUses)
	c.JSON(200, &res.InviteCreationResponse{ID: id, Code: code})
}

func (h *Handler) PerformGetInviteInfo(c *gin.Context) {
	// user := c.MustGet("user").(*info.RequestInfo)
	invite := c.Param("invite")
	id, guild, inviterid, inviterserver, uses, maxuses := h.db.GetInviteByCode(invite)
	if id == 0 {
		c.Status(http.StatusNotFound)
	} else {
		c.JSON(http.StatusOK, res.InviteInfoResponse{ID: id, Guild: guild, InviterID: inviterid, InviterServer: inviterserver, Uses: uses, MaxUses: maxuses})
	}
}
