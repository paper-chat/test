package ws

import (
	"log"

	"gitlab.com/paper-chat/test/db"
)

// Hub maintains the set of active clients and Broadcasts messages to the
// clients.
type Hub struct {
	// Registered clients.
	clients map[*Client]bool

	// Inbound messages from the clients.
	NewMessage chan *Message

	// Register requests from the clients.
	register chan *Client

	// Unregister requests from clients.
	unregister chan *Client
	db         db.DB
}

func NewHub(db db.DB) *Hub {
	return &Hub{
		NewMessage: make(chan *Message),
		register:   make(chan *Client),
		unregister: make(chan *Client),
		clients:    make(map[*Client]bool),
		db:         db,
	}
}

func (h *Hub) Run() {
	for {
		select {
		case client := <-h.register:
			h.clients[client] = true
		case client := <-h.unregister:
			if _, ok := h.clients[client]; ok {
				delete(h.clients, client)
				close(client.send)
			}
		case message := <-h.NewMessage:
			for client := range h.clients {
				if h.db.IsUserInGuild(client.info.ID, client.info.Server, message.GuildID) {
					select {
					case client.send <- *message:
					default:
						close(client.send)
						delete(h.clients, client)
					}
				} else {
					log.Printf("User %d not in guild %d", client.info.ID, message.GuildID)
				}
			}
		}
	}
}
