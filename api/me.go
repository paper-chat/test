package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/paper-chat/test/api/info"
	"gitlab.com/paper-chat/test/api/requests"
)

func (h *Handler) PerformSaveMyGuild(c *gin.Context) {
	user := c.MustGet("user").(*info.RequestInfo)
	if user.Server == h.config.ServerID {
		var req requests.GuildSaveRequest
		if err := c.ShouldBindJSON(&req); err != nil {
			c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
			return
		}
		h.db.SaveUsersGuild(user.ID, req.ID, req.Server, req.Key)
		return
	}
	c.AbortWithStatus(401)
}
