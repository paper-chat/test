package api

import (
	"net/http"

	"github.com/gin-gonic/gin"
	"gitlab.com/paper-chat/test/api/requests"
	res "gitlab.com/paper-chat/test/api/responses"
)

func (h *Handler) PerformCreateCategory(c *gin.Context) {
	var req requests.CategoryCreationRequest
	if err := c.ShouldBindJSON(&req); err != nil {
		c.JSON(http.StatusBadRequest, gin.H{"error": err.Error()})
		return
	}
	guild := c.MustGet("guild").(uint64)
	id := h.db.CreateCategory(guild, req.Name, req.Description)
	c.JSON(200, &res.IDResponse{ID: id})
}

func (h *Handler) PerformDeleteCategory(c *gin.Context) {
	category := c.MustGet("category").(uint64)
	h.db.DeleteCategory(category)
	c.Status(200)
}
