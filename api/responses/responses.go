package responses

type ResultResponse struct {
	Email    string `json:"email"`
	Username string `json:"username"`
	Password string `json:"password"`
}

type LoginResponse struct {
	ID    uint64 `json:"id"`
	Token string `json:"token"`
}

type GetTokensResponse struct {
	Tokens []Token `json:"tokens"`
}

type IDResponse struct {
	ID uint64 `json:"id"`
}

type InviteCreationResponse struct {
	ID   uint64 `json:"id"`
	Code string `json:"code"`
}

type InviteInfoResponse struct {
	ID            uint64 `json:"id"`
	Guild         uint64 `json:"guild"`
	InviterID     uint64 `json:"inviterid"`
	InviterServer string `json:"inviterserver"`
	Uses          uint32 `json:"uses"`
	MaxUses       uint32 `json:"maxuses"`
}
