package responses

type Token struct {
	Token  string `json:"token"`
	Client string `json:"client"`
	Type   string `json:"type"`
	Origin string `json:"origin"`
}

type Guild struct {
	ID          uint64 `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
}

type Channel struct {
	ID          uint64 `json:"id"`
	Name        string `json:"name"`
	Description string `json:"description"`
	Category    uint64 `json:"category"`
}
