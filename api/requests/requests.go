package requests

type AccountCreationRequest struct {
	Email    string `json:"email" binding:"required"`
	Username string `json:"username" binding:"required"`
	Password string `json:"password" binding:"required"`
}

type LoginRequest struct {
	Email    string `json:"email" binding:"required"`
	Password string `json:"password" binding:"required"`
	Type     string `json:"type" binding:"required"`
}

type GuildCreationRequest struct {
	Name        string `json:"name" binding:"required"`
	Description string `json:"description" binding:"required"`
}

type GuildUpdateRequest struct {
	Name        string `json:"name" binding:"required"`
	Description string `json:"description" binding:"required"`
}

type ChannelCreationRequest struct {
	Name        string `json:"name" binding:"required"`
	Description string `json:"description" binding:"required"`
	Category    uint64 `json:"category" binding:"required"`
}

type GuildSaveRequest struct {
	ID     uint64
	Server string
	Key    string
}

type MessageCreationRequest struct {
	Content string `json:"content" binding:"required"`
	//ReplyID uint64 `json:"replyid"`
}

type MessageUpdateRequest struct {
	Content string `json:"content" binding:"required"`
	//ReplyID uint64 `json:"replyid"`
}

type CategoryCreationRequest struct {
	Name        string `json:"name" binding:"required"`
	Description string `json:"description" binding:"required"`
}

type InviteCreationRequest struct {
	MaxUses uint32 `json:"maxuses" binding:"required"`
	// ValidUntil time.Time
}
