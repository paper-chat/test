package main

import (
	"log"
	"os"

	"gitlab.com/paper-chat/test/api"
	"gitlab.com/paper-chat/test/config"
	"gitlab.com/paper-chat/test/db"
	"gitlab.com/paper-chat/test/utils"
	"go.uber.org/fx"
)

func main() {
	mode := "api"
	var APIModule = fx.Options(
		fx.Provide(
			newLogger,
			newSnowflakeNode,
		),
		api.Module,
		utils.Module,
		config.Module,
		db.Module,
		fx.Invoke(api.RegisterHandlers),
	)
	if mode == "api" {
		app := fx.New(
			APIModule,
		)
		app.Run()
	} else {
		panic("Unsupported mode")
	}
}

func newLogger() *log.Logger {
	logger := log.New(os.Stdout, "" /* prefix */, 0 /* flags */)
	return logger
}
